<?php $this->load->view('includes/headerMain') ?>
<header id="banner">
    <div class="container" id="banner_container">
        <h3 class="banner-title">Login / Register</h3>
        <p class="banner-subtitle"></p>
    </div>
</header>
<div class="content-wrapper clearfix">
    <div class="container"><!-- container via hooks -->	<div id="page-content-container">	
            <div class="row-fluid">
                <div class="span4 offset1">
                    <div class="form-container">
                        <?= $this->load->view('predesign/login') ?>
                    </div>
                </div>
                <div class="span4 offset1">
                    <div class="form-container">
                        <?= $output ?>
                    </div>
                </div>
            </div>
        </div>

    </div><!-- /.container via hooks-->		</div><?php $this->load->view('includes/footer') ?>