
<div class="container-full">
    <?php $this->load->view('includes/headerMain') ?>

    <header id="banner">
        <div id="banner_container" class="container">
            <h3 class="banner-title">Contacte</h3>
            <p class="banner-subtitle"></p>
        </div>
    </header>			
    <div class="content-wrapper clearfix">
        <div class="container"><!-- container via hooks -->
            <div id="contact-18" class="post-18 page type-page status-publish hentry">
                <div id="property_map" style='width:100%; height:300px;'></div>
                <div class="row-fluid clearfix">
                    <div class="span9">
                        <div class="form-wrapper">
                            <div class="contact-form-title"><?= $this->ajustes->titulo_contacto ?></div>
                            <div class="contact-form-excerpt"><?= $this->ajustes->texto_contacto ?></div>
                            <form id="contact-page-form" action="" onsubmit="return sendMail()" method="post">
                                <div class="controls controls-row">
                                    <input class="span4" id="nombre" placeholder="*El teu nom " required="" type="text">
                                    <input class="span4" id="email" placeholder="*El teu email" required="" type="email">
                                    <input class="span4" id="web" placeholder="El teu web" type="text">
                                </div>
                                <div class="controls">
                                    <textarea id="mensaje" class="span12" rows="10" placeholder="*Missatge" required=""></textarea>
                                </div>
                                <div class="controls">
                                    <input id="submit" class="btn btn-contact" value="Enviar" type="submit">                                    
                                </div>
                                <div style="display: none;" id="contact-message" class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>Gracies</strong>  El teu missatge s’ha enviat
                                </div>
                            </form>
                        </div><!-- form-wrapper -->
                    </div><!-- /.span9 -->
                    <div class="span3">
                        <div class="contact-page-info">
                            <address>
                                <div class="company-name"><?= $this->ajustes->establecimiento_contacto ?></div>
                                <div class="company-address"><?= $this->ajustes->direccion_contacto ?></div>
                                <div class="company-phone"><i class="fa fa-phone"></i><?= $this->ajustes->telefono ?> </div>
                                <div class="company-email"><i class="fa fa-envelope"></i><a href="mailto:<?= $this->ajustes->correo ?>"><?= $this->ajustes->correo ?></a></div>                                
                            </address>
                        </div><!-- /.contact-page-info -->
                    </div><!-- /.span3 -->
                </div><!-- /.row-fluid -->
            </div><!-- /#contact -->
        </div><!-- /.container via hooks-->
    </div><!-- /.content-wrapper -->
    <?php $this->load->view('includes/footer') ?>
</div>
<script src="http://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places,geometry&sensor=true"></script>
<script>    
    <?php 
        $map = $this->ajustes->mapa_contacto;
        $map = str_replace('(','',$map);
        $map = str_replace(')','',$map);
        $map = explode(',',$map);
        echo 'var lat = "'.$map[0].'", lon = "'.$map[1].'"; ';
    ?>
    var mapOptions = {
        zoom: 9,
        center: new google.maps.LatLng(lat,lon)
    };   
    map = new google.maps.Map(document.getElementById('property_map'), mapOptions);   
    new google.maps.Marker({ position: new google.maps.LatLng(lat,lon), map: map, title: 'TEST' });
    
    function sendMail(){
        var data = {};
        data.nombre = $("#nombre").val();
        data.email = $("#email").val();
        data.web = $("#web").val();
        data.mensaje = $("#mensaje").val();
        $.post('main/contacto',data,function(data){
            $("#contact-message").show();
        });
        return false;
    }
</script>