<div class="container-full">					
    <?php $this->load->view('includes/headerMain') ?>
    <div class="content-wrapper clearfix">
        <div id="headline_wrapper" style="margin-bottom: 0px;">
            <div id="headline_container" class="container">
                <section class="headline-title">
                    <h3>La teva immobiliaria de més de luxe.</h3>
                    <p>
                        Oferim les millors propietats que necesiten els més exigent.
                    </p>
                </section>
                <section class="property-search">
                    <div class="row-fluid">
                        <div class="span5">

                        </div>
                        <div class="span7">
                            <div class="search-wrapper">
                                <div class="search-title">Buscar</div>
                                <div class="search-form-v1">
                                    <span class="search-or"></span>
                                    <p class="search-info">població, comarca, C.P. (separat amb comes)</p>
                                    <?= $this->load->view('includes/searchboxMain') ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <div id="property_list">		
            <div class="row-fluid">                
                <div class="span12" id="map" style="height:400px">
                    
                </div>
            </div>
        </div><!-- /#property_list -->
        <div id="property_list" class="container">		
            <div id="title-listing" class="container">
                <div class="property-list-title">Darreres Propietats</div>
                <div class="property-list-by">
                    
                    <a class="current" href="<?= site_url() ?>">Tots</a>
                    <?php foreach($this->categorias->result() as $c): ?>
                        <a class="" href="<?= site_url('propiedad/lista').'?categorias_id='.$c->id ?>"><?= $c->categorias_nombre ?></a>
                    <?php endforeach ?>
                </div>
            </div><!-- /#title-listing -->            
                <?php foreach($propiedades->result() as $n=>$p): ?>
                    <?php if($n==0 || $n==4 || $n==8): ?>
                        <div class="row-fluid property-row">
                    <?php endif ?>
                        <?php $this->load->view('includes/fragmentos/item',array('detail'=>$p)); ?>
                    <?php if($n==3 || $n==7 || $n==$propiedades->num_rows-1): ?>
                        </div>
                    <?php endif ?>
                <?php endforeach ?>                
        </div><!-- /#property_list -->
        <div id="property_info">
            <div class="container carousel-wrapper">
                <div class="container" id="recent-title-listing">
                    <div class="recent-property-list-title">Propietats Recents</div>
                    <div class="recent-property-list-by">
                        <div class="jcarousel-control">
                            <a class="jcarousel-control-prev" href="#" data-jcarouselcontrol="true" style="background:white;color:#7C888E;padding:7px 8px; border-radius:2px">
                                    <i class="fa fa-chevron-left"></i>
                            </a>
                            <a class="jcarousel-control-next" href="#" data-jcarouselcontrol="true" style="background:white;color:#7C888E;padding:7px 8px; border-radius:2px">
                                    <i class="fa fa-chevron-right"></i>
                            </a>
                        </div>
                    </div>
                </div><!-- /#title-listing -->
                <div class="jcarousel container" data-jcarousel="true" data-jcarouselautoscroll="true">
                    <div class="jcontainer" style=" top: 0px;">                        
                        <?php foreach($recientes->result() as $n=>$p): ?>
                                <?php $this->load->view('includes/fragmentos/recientesMain',array('detail'=>$p)); ?>                            
                        <?php endforeach ?>
                        <?php if($recientes->num_rows==0): ?>
                            No hi han propietats
                        <?php endif ?>
                    </div><!-- jcontainer -->
                </div><!-- /.jcarousel -->
            </div><!-- /.container -->
        </div>
        <div id="property_partner">
            <div class="container">
                <header class="partner-header">
                    <h3 class="partner-title">Ciutats</h3>
                </header>
                <p class="partner-excerpt">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vitae fermentum diam, vitae gravida eros. Proin interdum imperdiet
                </p>
                <div id="partners_slider" class="partners-logo-wrapper">
                    <div class="partner-list">
                        <?php foreach($partners->result() as $p): ?>
                            <div class="partner-item">                                
                                <img width="170" height="55" src="<?= base_url('uploads/partners/'.$p->foto) ?>" class="attachment-partners-thumb wp-post-image" alt="graphicriver" title="graphicriver" />                                
                            </div>
                        <?php endforeach ?>
                    </div>
                    <div class="partner-control">
                        <a href="#" class="partner-prev" style="color:black; background:#6A7982;color:white;padding:3px 8px; border-radius:2px"><i class="fa fa-chevron-left"></i></a>
                        <a href="#" class="partner-next" style="color:black; background:#6A7982;color:white;padding:3px 8px; border-radius:2px"><i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
        </div><!-- /#property_partner -->	
    </div><!-- /.content-wrapper -->
    <?php $this->load->view('includes/footer') ?>
</div><!-- .container-full -->
<script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', initMap); 
    function initMap() {
        var mapOptions = {
            zoom: 11, 
            scrollwheel: false,
            zoomControl: true, 
            mapTypeControl: false, 
            scaleControl: false, 
            streetViewControl: false, 
            center: new google.maps.LatLng(39.67442740076737,3.0157470703125), 
            styles: [
                { "featureType": "water", "elementType": "geometry", "stylers": [ { "color": "#ededed" }, { "lightness": 17 } ] }, 
                { "featureType": "landscape", "elementType": "geometry", "stylers": [ { "color": "#f7f7f7" }, { "lightness": 20 } ] },
                { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [ { "color": "#ffffff" }, { "lightness": 17 } ] },
                { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [ { "color": "#ffffff" }, { "lightness": 29 },{ "weight": 0.2 } ] }, 
                { "featureType": "road.arterial", "elementType": "geometry", "stylers": [ { "color": "#ffffff" }, { "lightness": 18 } ] },
                { "featureType": "road.local", "elementType": "geometry", "stylers": [ { "color": "#ffffff" }, { "lightness": 16 } ] }, 
                { "featureType": "poi", "elementType": "geometry", "stylers": [ { "color": "#f5f5f5" }, { "lightness": 21 } ] }, 
                { "featureType": "poi.park", "elementType": "geometry", "stylers": [ { "color": "#dedede" }, { "lightness": 21 } ] }, 
                { "elementType": "labels.text.stroke", "stylers": [ { "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 } ] },
                { "elementType": "labels.text.fill", "stylers": [ { "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 } ] },
                { "elementType": "labels.icon", "stylers": [ { "visibility": "off" } ] }, 
                { "featureType": "transit", "elementType": "geometry", "stylers": [ { "color": "#f2f2f2" }, { "lightness": 19 } ] }, 
                { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [ { "color": "#fefefe" }, { "lightness": 20 } ] }, 
                { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [ { "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 } ] } 
            ] 
        }; 
        var mapElement = document.getElementById('map'); 
        var map = new google.maps.Map(mapElement, mapOptions);
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {            
            map.setOptions({ 'draggable': false });
        }
        var image = '<?= base_url('images/map-locator.png') ?>'; 
        var markers = [];

        var bounds = new google.maps.LatLngBounds();
        <?php foreach($this->db->get('propiedades')->result() as $h): ?>
            var marker<?= $h->id ?> = new google.maps.Marker({ 
                /*icon: image,*/
                animation: google.maps.Animation.DROP, 
                position: new google.maps.LatLng<?= $h->ubicacion ?>, 
                map: map, 
                title: '<?= $h->nombre_propiedad ?>' 
            });
            bounds.extend(new google.maps.LatLng<?= $h->ubicacion ?>);
            google.maps.event.addListener(marker<?= $h->id ?>,'click',function(){
                document.location.href="<?= site_url('propiedad/'.toURL($h->nombre_propiedad).'-'.$h->id) ?>/";
            });
        <?php endforeach ?>
        map.fitBounds(bounds);
    } 
</script> <!-- youtube video player settings --> 