<?php if($this->user->log): ?>
<div id="sidebar" class="sidebar responsive">
        <ul class="nav nav-list">
            <li class="active highlight">
                <a href="<?= site_url('panel') ?>">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text">Escritorio</span>
                </a>
                <b class="arrow"></b>
            </li>            
            <!--- Admin --->
            <li>
                <a class="dropdown-toggle" href="#">
                        <i class="menu-icon fa fa-lock"></i>
                        <span class="menu-text">Admin</span>
                        <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <li>
                        <a href="<?= base_url('admin/ajustes') ?>">
                            <i class="menu-icon fa fa-users"></i>
                            <span class="menu-text">Ajustes</span>                                    
                        </a>                                
                    </li>
                    <li>
                        <a href="<?= base_url('admin/categorias') ?>">
                            <i class="menu-icon fa fa-users"></i>
                            <span class="menu-text">Categorías</span>                                    
                        </a>                                
                    </li>
                    <li>
                        <a href="<?= base_url('admin/subcategorias') ?>">
                            <i class="menu-icon fa fa-users"></i>
                            <span class="menu-text">SubCategorías</span>                                    
                        </a>                                
                    </li>
                    <li>
                        <a href="<?= base_url('admin/agentes') ?>">
                            <i class="menu-icon fa fa-users"></i>
                            <span class="menu-text">Agentes</span>                                    
                        </a>                                
                    </li>
                    <li>
                        <a href="<?= base_url('admin/propiedades') ?>">
                            <i class="menu-icon fa fa-users"></i>
                            <span class="menu-text">Propiedades</span>                                    
                        </a>                                
                    </li>
                    <li>
                        <a href="<?= base_url('admin/partners') ?>">
                            <i class="menu-icon fa fa-users"></i>
                            <span class="menu-text">Partners</span>                                    
                        </a>                                
                    </li>
                    <li>
                        <a href="<?= base_url('admin/comarcas') ?>">
                            <i class="menu-icon fa fa-users"></i>
                            <span class="menu-text">Comarcas</span>                                    
                        </a>                                
                    </li>
                    <li>
                        <a href="<?= base_url('blog/admin/blog') ?>">
                            <i class="menu-icon fa fa-users"></i>
                            <span class="menu-text">Blog</span>                                    
                        </a>                                
                    </li>
                    <!--- Seguridad --->
                    <li>
                        <a class="dropdown-toggle" href="#">
                            <i class="menu-icon fa fa-lock"></i>
                            <span class="menu-text">Seguridad</span>
                            <b class="arrow fa fa-angle-down"></b>
                        </a>
                        <b class="arrow"></b>
                        <ul class='submenu'>
                            <li>
                                <a href="<?= base_url('seguridad/grupos') ?>">
                                    <i class="menu-icon fa fa-users"></i>
                                    <span class="menu-text">Grupos</span>                                    
                                </a>                                
                            </li>
                            <li>
                                <a href="<?= base_url('seguridad/funciones') ?>">
                                    <i class="menu-icon fa fa-arrows"></i>
                                    <span class="menu-text">Funciones</span>                                    
                                </a>                                
                            </li>
                            <li>
                                <a href="<?= base_url('seguridad/user') ?>">
                                    <i class="menu-icon fa fa-user"></i>
                                    <span class="menu-text">Usuarios</span>                                    
                                </a>                                
                            </li>
                        </ul>
                    </li>
                    <!--- Fin seguridad ---->
                </ul>
            </li>
            <!--- Fin Admin ---->
        </ul>
       <div id="sidebar-collapse" class="sidebar-toggle sidebar-collapse">
            <i data-icon2="ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" class="ace-icon fa fa-angle-double-left"></i>
        </div>

        <script type="text/javascript">
                try{ace.settings.check('sidebar' , 'collapsed');
                ace.settings.sidebar_collapsed(true, true);
                }catch(e){}
        </script>
</div>
<?php endif ?>
