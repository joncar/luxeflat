<section id="footer_widgets">
        <div id="footer_widget_wrapper">
            <div class="container">
                <div class="row-fluid">
                    <div id="text-2" class="span6 widget widget_text">
                        <h3 class="widget-title" style="font-size:22px;">Sobre nosaltres</h3>
                        <div class="textwidget" style="font-size:17px;">
                            <p><?= img('images/footer2.jpg','width:9%') ?> <br/>Som una immobiliària especialitzada en la compra, venda i lloguer de sòls industrials. Oferim assessorament personalitzat perquè trobis l'opció que millor s'adapta a les teves necessitats. Més de 15 anys d'experiència avalen la nostra professionalitat. 
                                Empresa col·legiada a l'Associació Professional d'Experts Immobiliaris amb el núm. de col·legiat 5281</p>
                        </div>
                    </div>
                    <div id="pages-2" class="span2 widget widget_pages">
                        <h3 class="widget-title">Links</h3>
                        <ul>
                            <li class="page_item page-item-14"><a href="<?= site_url('propiedad/lista').'?categorias_id=2' ?>">Pisos</a></li>
                            <li class="page_item page-item-14"><a href="<?= site_url('propiedad/lista').'?categorias_id=1' ?>">Cases</a></li>
                            <li class="page_item page-item-18"><a href="<?= site_url('propiedad/lista').'?tipo_venta=2' ?>">Locals lloguer</a></li>
                            <li class="page_item page-item-115"><a href="<?= site_url('propiedad/lista').'?tipo_venta=1' ?>">Locals venda</a></li>
                            <li class="page_item page-item-1864"><a href="<?= site_url('actualitat') ?>">Actualitat</a></li>
                            <li class="page_item page-item-1681"><a href="<?= site_url('contacte') ?>">Contacte</a></li>                            
                        </ul>
                    </div>
                    <div id="blog-widget-2" class="span4 widget blog">
                        <h3 class="widget-title">Actualitat</h3>
                        <ul class="footer-blog">
                            <?php foreach($this->footerentrys->result() as $f): ?>
                            <li>
                                <a href="<?= site_url('actualitat/'.  toURL($f->id.'-'.$f->titulo)) ?>">
                                    <img width="70" height="60" src="<?= base_url('application/modules/blog/images/fotos/'.$f->foto) ?>" class="alignleft" alt="" />
                                </a>
                                <a href="<?= site_url('actualitat/'.  toURL($f->id.'-'.$f->titulo)) ?>">
                                    <?= $f->titulo ?>
                                </a><br />
                                <span class="blog-date"><?= date("d-m-Y",strtotime($f->fecha)) ?></span>
                            </li>
                            <?php endforeach ?>                            
                        </ul>
                    </div> 
                </div>
            </div>
        </div><!-- /.footer-widget-wrapper -->
    </section><!-- #footer_widgets -->
    <footer id="footer">
        <div class="container cleafix">
            <div class='span9'>
                <p class="pull-left">Copyright 2016, Tots els drets reservats per Espais Industrials.</p>
                <div class="pull-right">
                    <ul class="footer-social">
                        <li><a href="http://www.facebook.com/" title="Facebook"><i class="icon-facebook"></i></a></li>
                        <li><a href="http://www.twitter.com/" title="Twitter"><i class="icon-twitter"></i></a></li>
                        <li><a href="http://feeds.feedburner.com/" title="RSS"><i class="icon-rss"></i></a></li>							
                        <li><a href="http://www.plus.google.com/" title="Google Plus"><i class="icon-google-plus"></i></a></li>														
                    </ul>
                </div>
            </div>
            <div class="span2 hidden-phone" align='right'>
                <a href='http://www.apibcn.com/es'><?= img('images/footer.jpg','width:47%') ?></a>                
            </div>
            <div class="span2 hidden-desktop hidden-tablet" align='right'>
                <a href='http://www.apibcn.com/es'><?= img('images/footer.jpg','width:17%') ?></a>                
            </div>
       </div>
    </footer>