<div class="nav-background">
        <div class="container">
            <div class="clearfix navbar">
                <div class="navbar-inner">
                    <div class="container">
                        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </a>
                        <div class="nav-collapse collapse">
                            <nav id="nav-main" role="navigation">
                                <ul id="menu-primary-menu" class="nav">
                                    <li class="current_page_item"><a href="<?= site_url() ?>">Inici</a></li>
                                    <li class="dropdown"><a class="dropdown-toggle" href="<?= site_url('propiedad/lista') ?>?categorias_id=17">Pisos</a>
                                            <ul class="dropdown-menu">
                                                    <li><a href="<?= site_url('propiedad/lista') ?>?categorias_id=17&subcategorias_id=">Pis</a></li>
                                                    <li><a href="<?= site_url('propiedad/lista') ?>?categorias_id=16&subcategorias_id=">Dúplex</a></li>
                                                    <li><a href="<?= site_url('propiedad/lista') ?>?categorias_id=15&subcategorias_id=">Àtic</a></li>
                                            </ul>
                                    </li>
                                    <li class="dropdown"><a class="dropdown-toggle" href="<?= site_url('propiedad/lista') ?>?categorias_id=14">Cases</a>
                                            <ul class="dropdown-menu">
                                                    <li><a href="<?= site_url('propiedad/lista') ?>?categorias_id=14&subcategorias_id=">Casa</a></li>
                                                    <li><a href="<?= site_url('propiedad/lista') ?>?categorias_id=13&subcategorias_id=">Adossada</a></li>
                                            </ul>
                                    </li>
                                    <li class="dropdown"><a class="dropdown-toggle" href="<?= site_url('propiedad/lista') ?>?categorias_id=11">Locals</a>
                                            <ul class="dropdown-menu">
                                                    <li><a href="<?= site_url('propiedad/lista') ?>?categorias_id=2&tipo_venta=2">Venda</a></li>
                                                    <li><a href="<?= site_url('propiedad/lista') ?>?categorias_id=2&tipo_venta=1">Lloguer</a></li>
                                            </ul>
                                    </li>
                                    <li><a href="<?= site_url('propiedad/listado') ?>">Llistat</a></li>
                                    <li><a href="<?= site_url('actualitat') ?>">Actualitat</a></li>
                                    <li><a href="<?= site_url('contacte') ?>">Contacte</a></li>
                                </ul>
                                <div id="social-network">
                                    <a class="fb" href="<?= $this->ajustes->facebook ?>" title="Facebook"><i class="fa fa-facebook"></i></a>
                                    <a class="tw" href="<?= $this->ajustes->twitter ?>" title="Twitter"><i class="fa fa-twitter"></i></a>
                                    <a class="rss" href="<?= $this->ajustes->instagram ?>" title="Linkedin"><i class="fa fa-linkedin"></i></a>					
                                    <a class="gp" href="<?= $this->ajustes->google ?>" title="Google Plus"><i class="fa fa-google-plus"></i></a>	
                                </div>
                            </nav> <!-- #nav-main -->
                        </div>
                    </div>
                </div>
            </div><!-- .navbar -->
        </div>
    </div>