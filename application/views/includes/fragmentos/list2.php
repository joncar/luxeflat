<div class="dsidx-media">
    <div class="dsidx-photo"><a href="<?= site_url('propiedad/'.toURL($detail->nombre_propiedad).'-'.$detail->id) ?>">
            <img src="<?= base_url('uploads').'/'.$detail->foto_portada ?>" alt="<?= $detail->nombre_propiedad ?>" title="<?= $detail->nombre_propiedad ?>"></a>
    </div>
</div>
<div class="dsidx-primary-data">
    <div class="dsidx-address"><a href="<?= site_url('propiedad/'.toURL($detail->nombre_propiedad).'-'.$detail->id) ?>">
        <?= $detail->nombre_propiedad ?></a>
    </div>
    <div class="dsidx-price">
        <?= is_numeric($detail->precio) && $detail->precio!=0?number_format($detail->precio,0,'.','.').'<sup class="price-curr">€</sup>':'A convenir'; ?>
    </div>
</div>
<div class="dsidx-secondary-data">
    <?php if($detail->categorias_id==11): ?>
        <?php if($detail->metros_planta!=null): ?>
            <span class="meta-exterior" title="metres planta"><i class="ico-size-floor"></i><?= $detail->metros_planta ?>M<sup class="size-curr">2</sup></span>
        <?php endif ?>
        <?php if($detail->altura!==null): ?>
            <span class="meta-bedroom" title="alçada"><i class="ico-bedroom"></i><?= $detail->altura ?>M</span>
        <?php endif ?>
        <?php if($detail->oficinas==1): ?>
            <span class="meta-bathroom" title="Oficines"><i class="ico-bathroom"></i></span>
        <?php endif ?>
        <?php if($detail->instalacion_electiva==1): ?>
            <span class="meta-eletric" title="Instal.lació elèctrica"><i class="ico-electric"></i></span>
        <?php endif ?>
        <?php if($detail->lavabos==1): ?>
            <span class="meta-bath" title="Lavabos"><i class="ico-bath"></i></span>
        <?php endif ?>
        <?php if($detail->patio==1): ?>
            <span class="meta-exterior" title="Pati"><i class="ico-exterior"></i></span>
        <?php endif ?>
    <?php else: ?>
        <?php if($detail->habitaciones!==null): ?>
            <span class="meta-bedroom" title="#Habitaciones"><i class="ico-bedroom"></i><?= $detail->habitaciones ?></span>
        <?php endif ?>
        <?php if($detail->banios!==null): ?>
            <span class="meta-bedroom" title="#Banys"><i class="ico-bedroom"></i><?= $detail->banios ?></span>
        <?php endif ?>
        <?php if($detail->ascensor==1): ?>
            <span class="meta-bathroom" title="Ascensor"><i class="ico-bathroom"></i></span>
        <?php endif ?>
        <?php if($detail->parking==1): ?>
            <span class="meta-bathroom" title="Parking"><i class="ico-bathroom"></i></span>
        <?php endif ?>
        <?php if($detail->terraza==1): ?>
            <span class="meta-bathroom" title="Terraza"><i class="ico-bathroom"></i></span>
        <?php endif ?>
        <?php if($detail->planta==1): ?>
            <span class="meta-bathroom" title="Planta"><i class="ico-bathroom"></i></span>
        <?php endif ?>
        <?php if($detail->piscina==1): ?>
            <span class="meta-bathroom" title="Piscina"><i class="ico-bathroom"></i></span>
        <?php endif ?>
    <?php endif ?>
        <div title="Dirección"><i style="color: rgb(234, 77, 57); font-size: 30px;" class="fa fa-map-marker"></i>
            <?= $detail->direccion ?>
            <a href="<?= site_url('propiedad/'.toURL($detail->nombre_propiedad).'-'.$detail->id) ?>#property_map" role="button" target="_blank">Veure mapa</a>
        </div>    
</div>