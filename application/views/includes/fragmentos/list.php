<div class="row-fluid property-listing <?= $detail->tipo_venta==1?'status-35':'status-28' ?> clearfix">
    <div class="listing-image span5">
        <div class="property-image-container">
            <a href="<?= site_url('propiedad/'.toURL($detail->nombre_propiedad).'-'.$detail->id) ?>" title="<?= $detail->nombre_propiedad ?>">
                <img width="540" height="360" src="<?= base_url('uploads').'/'.$detail->foto_portada ?>" class="attachment-real-property-loop wp-post-image" alt="<?= $detail->nombre_propiedad ?>" title="<?= $detail->nombre_propiedad ?>" />
            </a>
        </div><!-- /.property-images-container -->
        <div class="listing-meta">
            <ul>
                <li class="meta-size" title="metres"><i class="ico-size"></i><?= $detail->metros ?>M<sup class="size-curr">2</sup></li>
                <?php if($detail->categorias_id==11): ?>
                    <?php if($detail->metros_planta!=null): ?>
                        <li class="meta-exterior" title="metres planta"><i class="ico-size-floor"></i><?= $detail->metros_planta ?>M<sup class="size-curr">2</sup></li>
                    <?php endif ?>
                    <?php if($detail->altura!==null): ?>
                        <li class="meta-bedroom" title="alçada"><i class="ico-bedroom"></i><?= $detail->altura ?>M</li>
                    <?php endif ?>
                    <?php if($detail->oficinas==1): ?>
                        <li class="meta-bathroom" title="Oficines"><i class="ico-bathroom"></i></li>
                    <?php endif ?>
                    <?php if($detail->instalacion_electiva==1): ?>
                        <li class="meta-eletric" title="Instal.lació elèctrica"><i class="ico-electric"></i></li>
                    <?php endif ?>
                    <?php if($detail->lavabos==1): ?>
                        <li class="meta-bath" title="Lavabos"><i class="ico-bath"></i></li>
                    <?php endif ?>
                    <?php if($detail->patio==1): ?>
                        <li class="meta-exterior" title="Pati"><i class="ico-exterior"></i></li>
                    <?php endif ?>
                <?php else: ?>
                    <?php if($detail->habitaciones!==null): ?>
                        <li class="meta-bedroom" title="#Habitaciones"><i class="ico-bedroom"></i><?= $detail->habitaciones ?></li>
                    <?php endif ?>
                    <?php if($detail->banios!==null): ?>
                        <li class="meta-bedroom" title="#Banys"><i class="ico-bedroom"></i><?= $detail->banios ?></li>
                    <?php endif ?>
                    <?php if($detail->ascensor==1): ?>
                        <li class="meta-bathroom" title="Ascensor"><i class="ico-bathroom"></i></li>
                    <?php endif ?>
                    <?php if($detail->parking==1): ?>
                        <li class="meta-bathroom" title="Parking"><i class="ico-bathroom"></i></li>
                    <?php endif ?>
                    <?php if($detail->terraza==1): ?>
                        <li class="meta-bathroom" title="Terraza"><i class="ico-bathroom"></i></li>
                    <?php endif ?>
                    <?php if($detail->planta==1): ?>
                        <li class="meta-bathroom" title="Planta"><i class="ico-bathroom"></i></li>
                    <?php endif ?>
                    <?php if($detail->piscina==1): ?>
                        <li class="meta-bathroom" title="Piscina"><i class="ico-bathroom"></i></li>
                    <?php endif ?>
                <?php endif ?>
            </ul>
        </div>
    </div>
    <div class="listing-info span7">
        <div class="listing-title">
            <a href="<?= site_url('propiedad/'.toURL($detail->nombre_propiedad).'-'.$detail->id) ?>" title="<?= $detail->nombre_propiedad ?>"><?= $detail->nombre_propiedad ?></a>
        </div>
        <div class="listing-content">
            <div class="listing-property-price">
                <?php if($detail->precio!=0): ?>
                <?= number_format($detail->precio,0,',','.') ?><sup class="price-curr">€</sup>&nbsp;
                <span class="price-postfix"></span>
                <?php else: ?>
                    A Convenir 
                <?php endif ?>
            </div>
            <div class="listing-excerpt">
                <p><?= $detail->descripcion ?></p>
            </div>
        </div>
        <div class="listing-address">
            <i style="color: rgb(234, 77, 57); font-size: 30px;" class="fa fa-map-marker"></i>
            <?= $detail->direccion ?>
            <a href="<?= site_url('propiedad/'.toURL($detail->nombre_propiedad).'-'.$detail->id) ?>#property_map" role="button" target="_blank">Veure mapa</a>
        </div>
    </div>
    <div class="property-status <?= $detail->tipo_venta==1?'status-35-text':'status-28-text' ?>"><?= $detail->tipo_venta==1?'Venda':'Lloguer' ?></div>
</div><!-- /.property-listing -->