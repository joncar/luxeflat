<div class="span3">
    <article class="property-item">
        <div class="property-images">
            <a title="<?= $detail->nombre_propiedad ?>" href="<?= site_url('propiedad/'.toURL($detail->nombre_propiedad).'-'.$detail->id) ?>">
                <img width="540" height="360" title="<?= $detail->nombre_propiedad ?>" class="<?= $detail->tipo_venta==1?'status-35 wp-post-image':'status-28 wp-post-image' ?>" src="<?= base_url('uploads').'/'.$detail->foto_portada ?>"></a>
            <div class="<?= $detail->tipo_venta==1?'property-status status-35-text':'property-status status-28-text' ?>"><?= $detail->tipo_venta==1?'Venda':'Lloguer' ?></div>
        </div><!-- /.property-images -->
        <div class="property-attribute">
            <h3 class="attribute-title"><a title="<?= $detail->nombre_propiedad ?>" href="<?= site_url('propiedad/'.toURL($detail->nombre_propiedad).'-'.$detail->id) ?>"><?= $detail->nombre_propiedad ?></a></h3>
            <span class="attribute-city"><?= $detail->direccion ?></span>
            <div class="attribute-price">
                <span class="attr-pricing"><?php if($detail->precio!=0): ?><?= number_format($detail->precio,0,',','.') ?><sup class="price-curr">€</sup><?php else: ?>A Consultar<?php endif ?></span>
            </div>
        </div>
        <div class="property-meta clearfix">
            <div  style="width:32%" class="meta-size meta-block" title="metres"><i class="ico-size"></i><span  style="width:32%" class="meta-text"><?= $detail->metros ?>M<sup class="size-curr">2</sup></span></div>
            <?php if($detail->categorias_id==11): ?>
                <?php if($detail->oficinas==1): ?>
                    <div  style="width:32%" class="meta-block meta-bathroom" title="Oficines"><i class="ico-bathroom"></i></div>
                <?php endif ?>
                <?php if($detail->patio==1): ?>
                    <div  style="width:32%" class="meta-block meta-exterior" title="Pati"><i class="ico-exterior"></i></div>
                <?php endif ?>
            <?php else: ?>
                <?php if($detail->habitaciones!==null): ?>
                    <div  style="width:32%" class="meta-block meta-bedroom" title="#Habitaciones"><i class="ico-bedroom"></i><?= $detail->habitaciones ?></div>
                <?php endif ?>
                <?php if($detail->banios!==null): ?>
                    <div  style="width:32%" class="meta-block meta-bedroom" title="#Banys"><i class="ico-bedroom"></i><?= $detail->banios ?></div>
                <?php endif ?>
            <?php endif ?>
        </div>
    </article>
</div>