<header id="header" role="banner">
    <div class="header-background">
        <div class="container">
            <div class="row-fluid">
                <div class="pull-left">
                    <a href="<?= site_url() ?>" title="Real Expert" rel="home">
                        <img class="site-logo" src="<?= base_url('images/logo.png') ?>" alt="Real Expert" />
                    </a>
                </div>
                <div class="pull-right">
                    <div class="top-menu">
                        <span class="contact-info"><i class="contact-email"></i><a href="#"><?= $this->ajustes->correo ?></a></span>
                        <span class="contact-info"><i class="contact-phone"></i><?= $this->ajustes->telefono ?></span>
                        <span class="contact-info" style="color:gray">
                            <?php if($_SESSION['lang']=="ca"): ?>
                                <a href="<?= base_url('main/traducir/es') ?>">                                    
                                    ESP
                                </a>
                            <?php else: ?>
                            ESP
                            <?php endif ?> | 
                            <?php if($_SESSION['lang']=="es"): ?>
                                <a href="<?= base_url('main/traducir') ?>">                                    
                                    CAT
                                </a>
                            <?php else: ?>
                            CAT
                            <?php endif ?>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('includes/nav') ?>
</header><!-- #header -->	