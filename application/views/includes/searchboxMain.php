<?php $formsubmit = empty($formsubmit)?base_url('propiedad/lista/'):$formsubmit; ?>
<form class="advance-search-form form-inline clearfix" action="<?= $formsubmit ?>" method="get" id="formSearchBox">
    <div class="option-bar location">
        <input class="input-location" type="text" id="direccion" name="direccion" placeholder="Igualada, Anoia, 08700"  value='<?= !empty($_GET['direccion'])?$_GET['direccion']:'' ?>'/>
    </div>
    <div class="option-bar type">
        <span class="selectwrap">
            <?php $sel = empty($_GET['categorias_id'])?0:$_GET['categorias_id']; ?>
            <?= form_dropdown_from_query('categorias_id','categorias','id','categorias_nombre',$sel,'id="select-cat"',FALSE,''); ?>   
        </span>
    </div>
    <div class="option-bar type">
        <span class="selectwrap">
            <?php $sel = empty($_GET['metros'])?0:$_GET['metros']; ?>
            <?= form_dropdown('metros',array(""=>"Metres/m2"),$sel,'id="select-metres" class=""'); ?>   
        </span>
    </div>
    <div class="option-bar type">
        <span class="selectwrap">
            <?php $sel = empty($_GET['banios'])?0:$_GET['banios']; ?>
            <?= form_dropdown('banios',array(""=>"#banios","1"=>"1","2"=>"2",'3'=>'3','4'=>'4'),$sel,'id="select-bathroom" class="search-select"'); ?>
        </span>
    </div>
    <div class="option-bar type">
        <span class="selectwrap">
            <?php $sel = empty($_GET['habitaciones'])?0:$_GET['habitaciones']; ?>
            <?= form_dropdown('habitaciones',array(""=>"#habitaciones","1"=>"1","2"=>"2",'3'=>'3','4'=>'4'),$sel,'id="select-bathroom" class="search-select"'); ?>
        </span>
    </div>
    <input type="hidden" name="page" value="<?= empty($_GET['page'])?'1':$_GET['page']-1 ?>" id="pageSearchBox">
    <input type="hidden" name="order" value="<?= empty($_GET['order'])?'id_ASC':$_GET['order'] ?>" id="order">
    <?php if($this->router->fetch_class()=='main'): ?>
        <div class="option-submit">
            <input type="submit" value="&nbsp;" class="advance-button-search">
        </div>
    <?php else: ?>
        <input type="submit" class="button button-search-widget" value="Buscar" />
    <?php endif ?>
</form>
<script>
    function changePage(val){
        $("#pageSearchBox").val(val);
        $("#formSearchBox").submit();
    }
    
    function changeTipo(id){
        $("#select-tipoventa").val(id);
        $("#formSearchBox").submit();
    }
    
    function changeCat(id){
        $("#select-cat").val(id);
        $("#formSearchBox").submit();
    }
    
    function changeOrder(i){
        $("#order").val(i);
        $("#formSearchBox").submit();
    }
    
    function actualizarMetros(valor){
        var valores = [];
        switch(valor){
            case '2':
                valores.push({id:'0-500',val:'0 a 500m2'});
                valores.push({id:'500-1500',val:'500 a 1.500m2'});
                valores.push({id:'1500-3000',val:'1.500 a 3.000m2'});
                valores.push({id:'3000-5000',val:'3.000 a 5.000m2'});
                valores.push({id:'5000',val:'més 5.000m2'});
            break;
            case '1':
                valores.push({id:'0-2000',val:'0 a 2.000m2'});
                valores.push({id:'2000-5000',val:'2.000 a 5.000m2'});
                valores.push({id:'5000-10000',val:'5.000 a 10.000m2'});
                valores.push({id:'10000-15000',val:'10.000 a 15.000m2'});
                valores.push({id:'20000',val:'més 20.000m2'});
            break;
        }
        var str = '<option value="">Metres/m2</option>';
        for(var i in valores){
            str+= '<option value="'+valores[i].id+'">'+valores[i].val+'</option>';
        }
        jQuery("#select-metres").html(str);
        jQuery("#select-metres").parents('.selectwrap').find('.selectbox-wrapper').remove();
        jQuery("#select-metres").parents('.selectwrap').find('.selectbox').remove();
        jQuery("#select-metres").selectbox();
    }
    
    function actualizarPrecio(valor){
        var valores = [];
        switch(valor){
            case '2':
                valores.push({id:'0-1000',val:'0 a 1.000€'});
                valores.push({id:'1000-2500',val:'1.000 a 2.500€'});
                valores.push({id:'2500-5000',val:'2.500 a 5.000€'});                
                valores.push({id:'5000',val:'més 5.000€'});
            break;
            case '1':
                valores.push({id:'100000-250000',val:'100.000 a 250.000€'});
                valores.push({id:'250000-500000',val:'250.000 a 500.000€'});                
                valores.push({id:'500000-1000000',val:'500.000 a 1.000.000€'});
                valores.push({id:'2000000',val:'més 2.000.000€'});
            break;
        }
        var str = '<option value="">Preu</option>';
        for(var i in valores){
            str+= '<option value="'+valores[i].id+'">'+valores[i].val+'</option>';
        }
        jQuery("#select-precio").html(str);
        jQuery("#select-precio").parents('.selectwrap').find('.selectbox-wrapper').remove();
        jQuery("#select-precio").parents('.selectwrap').find('.selectbox').remove();
        jQuery("#select-precio").selectbox();
    }
   
</script>

<script>
         jQuery(document).ready(function(){
            jQuery("#select-cat").selectbox({
                onChangeCallback:function(param){
                    actualizarMetros(param.selectedVal);
                }
            });
            actualizarMetros('<?= empty($_GET['categorias_id'])?0:$_GET['categorias_id'] ?>');
            
            jQuery("#select-tipoventa").selectbox({
                onChangeCallback:function(param){
                    actualizarPrecio(param.selectedVal);
                }
            });
            actualizarPrecio('<?= empty($_GET['tipo_venta'])?0:$_GET['tipo_venta'] ?>');
        });
    </script>