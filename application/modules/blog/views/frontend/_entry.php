<a href="<?= site_url('actualitat/'.toURL($detail->id.'-'.$detail->titulo)) ?>">
    <img src="<?= base_url('application/modules/blog/images/fotos/'.$detail->foto) ?>" alt="<?= $detail->titulo ?>" class="attachment-single-blog-post" height="450" width="870">
</a>
<article class="post">
    <header>
        <h2 class="entry-title">
            <a href="<?= site_url('actualitat/'.toURL($detail->id.'-'.$detail->titulo)) ?>" title="<?= $detail->titulo ?>" rel="bookmark">
                <?= $detail->titulo ?>
            </a>
        </h2>
        <div class="entry-meta">
            <span class="meta-parts">
                <time datetime="<?= date("Y-m-d",strtotime($detail->fecha))."T00:00:00+00:00"; ?>">
                    <i class="fa fa-calendar"></i><?= date("d/m/Y",strtotime($detail->fecha)); ?>
                </time>
            </span>
            <span class="meta-parts">
                <i class="fa fa-comment-o"></i>
                <a href="<?= site_url('actualitat/'.toURL($detail->id.'-'.$detail->titulo)) ?>#comments"><?= $detail->comentarios ?> Comentaris</a>
            </span>
        </div><!-- .entry-meta -->
    </header>

    <div class="entry-content clearfix">
        <p><?= substr(strip_tags($detail->texto),0,255) ?></p>
        <div class="read-more">
            <a href="<?= site_url('actualitat/'.toURL($detail->id.'-'.$detail->titulo)) ?>">Lleguir més&nbsp;<i class="icon-double-angle-right"></i></a>
        </div>
    </div><!-- .entry-content -->
</article>