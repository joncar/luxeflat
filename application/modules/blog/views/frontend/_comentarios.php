<ol class="commentlist unstyled">
    <?php foreach($comentarios->result() as $c): ?>
        <li class="media" id="comment-13">
            <span class="pull-left">
                <img alt="<?= $c->autor ?>" src="<?= base_url('assets/grocery_crud/css/jquery_plugins/cropper/vacio.png') ?>" class="avatar avatar-100 photo" height="100" width="100">
            </span>
            <div class="media-body">
                <div class="media-heading">
                    <cite>
                        <strong>
                            <a href="#" rel="external nofollow" class="url comment-author-link"><?= $c->autor; ?></a> 
                        </strong>
                    </cite>
                    <time datetime="<?= date("Y-m-d",strtotime($detail->fecha))."T00:00:00+00:00"; ?>" class="comment-date">
                        <?= date("D, d M Y",strtotime($detail->fecha)); ?>
                    </time>                
                </div>
                <div class="comment-content">
                    <p><?= $c->texto ?></p>
                </div><!-- .comment-content -->

                <div class="reply">                    
                    <a class="comment-reply-link" href="#respond">
                        Respondre&nbsp;<i class="icon-share-alt"></i>
                    </a>
                </div><!-- .reply -->
            </div> <!-- .media-body -->
        </li> <!-- .media -->
    <?php endforeach ?>
</ol> <!-- .commentlist -->