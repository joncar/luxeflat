<?php

class Traduccion{
    public $es = array(
        "Llistat de Propietats"=>"Lista de Propiedades",
        "Naus Industrials"=>"Naves Industriales",
        "Solars"=>"Solares",
        "Llistat"=>"Listado",
        "Actualitat"=>"Actualidad",
        "Contacte"=>"Contacto",
        "Amaga el mapa"=>"Oculta el mapa",
        "Ordenat per"=>"Ordenado por",
        "Propietats"=>"Propiedades",
        "Primer"=>"Primero",
        "Pròxim"=>"Siguiente",
        "Últim"=>"Último",
        "Últims Tweets"=>"Últimos Tweets",
        "Propietat"=>"Propiedad",
        "Tipus"=>"Tipo",
        "Metres"=>"Metros",
        "Preu"=>"Precio",
        "minim"=>"mínimo",
        "Màxim"=>"Máximo",
        "Oficines"=>"Oficinas",
        "Sobre nosaltres"=>"Sobre nosotros",
        "Naus industrials"=>"Naves industriales",
        "Lloguer"=>"Alquiler",
        "Venda"=>"Venta",
        "Tots els drets reservats per Espais Industrials"=>"Todos los derechos reservados por Espais Industrials",
        "adreça, població, comarca, C.P. (separat amb comes)"=>"dirección, población, comarca, C.P. (separado por comas)",
        "Darreres Propietats"=>"Últimas Propiedades",
        "Tots"=>"Todos",
        "Propietats Recents"=>"Propiedades Recientes",
        "Ciutats"=>"Ciudades",
        "No hi ha propietats"=>"No existen propiedades registradas",
        "No hi ha entrades"=>"No existen entradas registradas",
        "Instal.lació elèctrica"=>"Instalación eléctrica",
        "Alçada"=>"Altura",
        "Nau"=>"Nave",
        "Ubicació"=>"Ubicación",
        "Relacionats"=>"Relacionados",
        "Temps al mercat, la més recent primer"=>"Tiempo en el mercado, la más reciente primero",
        "Temps al mercat, la més antiga primer"=>"Tiempo en el mercado, la más antigua primero",
        "Preu, el més alt primer"=>"Precio, el más alto primero",
        "Preu, el més baix primer"=>"Precio, el más bajo primero",
        "Darreres"=>"Últimas",
        "Recents"=>"Recientes",
        "Comentaris"=>"Comentarios",
        "El teu nom"=>"Tu nombre",
        "Veure mapa"=>"Ver Mapa",
        "Poblacions, polígons, comarques..."=>"Poblaciones, polígonos, comarcas...",
        "Tenim una extensa llista de propietats en diferents polígons de localitats i comarques de Catalunya"=>"Tenemos una extensa lista de propiedades en diferentes poligonos de localidades y comarcas de Cataluña",
        "T'oferim els millors espais industrials per a la teva activitat"=>"Te ofrecemos los mejores espacios industriales para tu actividad",
        "Solares i naus industrials per a qualsevol activitat, en règim de lloguer o venda. Ens adaptem a les teves necessitats"=>"Solares y naves industriales para cualquier actividad, en régimen de alquiler o venta. Nos adaptamos a tus necesidades",
        "Som una immobiliària especialitzada en la compra, venda i lloguer de sòls industrials."=>"Somos una inmobiliaria especializada en la compra, venta y alquiler de suelos industriales.",
        "Oferim assessorament personalitzat perquè trobis l'opció que millor s'adapta a les teves necessitats."=>"Ofrecemos asesoramiento personalizado para que encuentres la opción que mejor se adapta a tus necesidades.",
        "Més de 15 anys d'experiència avalen la nostra professionalitat."=>"Más de 15 años de experiencia avalan nuestra profesionalidad.",
        "Empresa col·legiada a l'Associació Professional d'Experts Immobiliaris amb el núm."=>"Empresa colegiada a la Asociación Profesional de Expertos Inmobiliarios con el núm.",
        "de col·legiat 5281"=>"de colegiado 5281",
        "població, comarca, C.P. (separat amb comes)"=>"Población, comarca, C.P. (separado por comas)",
        "Pati"=>"Patio",
        "Detall de la propietat"=>"Detalle de la propiedad",
        "Agent"=>"Agente",
        "Recerca de Propiedades"=>"Busqueda de propiedades",
        "Ùltims Tweets"=>"Últimos Tweets"
    );

    public function traducir($view,$idioma = ''){
        if($idioma!='ca'){
            foreach($this->$idioma as $n=>$v){
                $view = str_replace($n,$v,$view);
            }
            return $view;
        }else{
            return $view;
        }
    }
}    
