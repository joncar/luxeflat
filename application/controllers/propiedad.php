<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('main.php');
class Propiedad extends Main {
        
        public function __construct()
        {
                parent::__construct();                                
                $propiedad = new Bdsource();
                $propiedad->limit = array(6,0);
                $propiedad->init('propiedades');  
                $this->inputs = array('categorias_id','tipo_venta','comarcas_id','direccion','nombre_propiedad','oficinas','order','page','metros','precio');
        }
        
        public function index($id = ''){
            if(empty($id)){
                throw new Exception('El inmueble buscado no se encuentra',404);
            }else{
                $id = explode('-',$id);
                if(count($id)>0 && is_numeric($id[count($id)-1])){
                    $id = $id[count($id)-1];
                    $propiedad = new Bdsource();
                    $propiedad->where = array('propiedades.id'=>$id);
                    $propiedad->join = array('agentes');
                    $propiedad->select = array('propiedades.*, agentes.nombre as agente, agentes.agente_foto, agentes.telefono as agente_telefono, agentes.correo as agente_correo');
                    $propiedad->init('propiedades',TRUE);
                    
                    $fotos = new Bdsource();
                    $fotos->where = array('propiedades_id'=>$this->propiedades->id);
                    $fotos->order_by = array('priority','ASC');
                    $fotos->init('fotos');                                       
                    
                    if($this->propiedades!=null){
                        $relacionadas = new Bdsource();
                        $relacionadas->limit = '4';
                        $relacionadas->where = array('categorias_id'=>$this->propiedades->categorias_id,'id != '=>$this->propiedades->id,'idioma'=>$_SESSION['lang']);
                        $relacionadas->init('propiedades',FALSE,'relacionadas');
                    
                        $this->loadView(array(
                            'view'=>'propiedad',
                            'title'=>$this->propiedades->nombre_propiedad,
                            'propiedad'=>$this->propiedades,
                            'relacionadas'=>$this->relacionadas,
                            'fotos'=>$this->fotos
                        ));
                    }else{
                        throw new Exception('El inmueble buscado no se encuentra',404);
                    }
                }else{
                    throw new Exception('El inmueble buscado no se encuentra',404);
                }
            }
        }
        
        function lista($view = '',$urlformparam = 'lista'){
            $prop = new Bdsource();
            $info = new Bdsource();
            if(!empty($_GET)){                
                foreach($_GET as $n=>$g){                    
                    if(!empty($g) && in_array($n,$this->inputs)){
                        if($n=='patio'){
                            $prop->where('patio',$g-1);
                            $info->where('patio',$g-1);
                        }elseif($n=='precio'){                            
                            $precio = explode('-',$g);
                            if(count($precio)==2){
                                $prop->where('precio >=',$precio[0]);
                                $info->where('precio >=',$precio[0]);
                                $prop->where('precio <=',$precio[1]);
                                $info->where('precio <=',$precio[1]);
                            }else{
                                $prop->where('precio >=',$precio[0]);
                                $info->where('precio >=',$precio[0]);
                            }
                        }elseif($n=='metros'){
                            $precio = explode('-',$g);
                            if(count($precio)==2){
                                $prop->where('metros >=',$precio[0]);
                                $info->where('metros >=',$precio[0]);
                                $prop->where('metros <=',$precio[1]);
                                $info->where('metros <=',$precio[1]);
                            }else{
                                $prop->where('metros >=',$precio[0]);
                                $info->where('metros >=',$precio[0]);
                            }
                        }elseif($n=='direccion'){
                            $prop->where('MATCH (propiedades.direccion) AGAINST ("'.$_GET['direccion'].'")',NULL);
                            $info->where('MATCH (propiedades.direccion) AGAINST ("'.$_GET['direccion'].'")',NULL);
                        }elseif($n=='oficinas'){
                            $prop->where($n,$g-1);
                            $info->where($n,$g-1);
                        }elseif($n=='page'){
                            $prop->limit = array('6',($g-1)*6);
                        }elseif($n=='order'){
                            $o = explode('_',$_GET['order']);
                            if(!empty($o[0]) && !empty($o[1])){
                                $prop->order_by = array($o[0],$o[1]);
                            }else{
                                unset($_GET['order']);
                            }
                        }else{
                            $prop->where($n,$g);
                            $info->where($n,$g);
                        }
                    }
                }
            }
            
            if(empty($_GET['page'])){
                $prop->limit = array('6','0');
                $_GET['page'] = 1;
            }
            if(empty($_GET['order'])){
                $prop->order_by = array('id','DESC');
                $_GET['order'] = 'id_DESC';
            }
            $prop->where('idioma',$_SESSION['lang']);
            $info->where('idioma',$_SESSION['lang']);
            $prop->init('propiedades',FALSE,'lista');
            $info->init('propiedades',FALSE,'totalItems');
            $totalPages = $this->totalItems->num_rows/6;
            $totalPages = explode('.',$totalPages);
            $totalPages = $totalPages[0];
            $totalPages = $totalPages==0?1:$totalPages;             
            $urlform = base_url('propiedad/'.$urlformparam.'/'.$view);
            $maxitem = ($_GET['page']-1)*6;
            
            $view = empty($view)?'list-list':$view;
            $this->loadView(array(
                'view'=>$urlformparam,
                'title'=>'Llistat de Propietats',
                'viewlist'=>$view,
                'propiedades'=>$this->lista,
                'total_pages'=> $totalPages,
                'urlform'=>$urlform,
                'current_page'=>$_GET['page'],
                'items'=>$maxitem
                ));
        }
}
/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
